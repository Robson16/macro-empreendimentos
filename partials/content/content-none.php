<?php
/*
 * Template Part to display that no posts were found
 */
?>

<div class="container">
    <h3><?php _e('No content to display', 'macro'); ?></h3>
    <?php get_search_form(); ?>
</div>
