wp.domReady(() => {
    wp.blocks.registerBlockStyle('core/group', {
        name: 'transparent-black',
        label: 'Transparent Black'
    });
    wp.blocks.registerBlockStyle('core/columns', {
        name: 'columns-with-border-right',
        label: 'Columns with Border Right'
    });
});