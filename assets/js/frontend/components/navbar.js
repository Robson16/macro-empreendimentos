export default class Navbar {
    constructor() {
        this.mainHeader = document.querySelector("#header");
        this.navbarToggler = document.querySelector(".navbar-toggler");
        this.dropdownLink = document.querySelectorAll(".dropdown > a");
        this.scrollY = window.scrollY;
        this.events();
    }

    // Events
    events() {
        this.navbarToggler.addEventListener("click", () => {
            let target = this.navbarToggler.dataset.target;
            this.handlerMobileMenuToggle(target);
        });

        this.dropdownLink.forEach((link) => {
            link.addEventListener("click", (event) => {
                event.preventDefault();
            });
        });

        window.onscroll = () => {
            this.scrollY = window.scrollY;

            if (this.scrollY > window.innerHeight * 0.33) {
                this.handlerShowFixedNavbar();
            } else {
                this.handlerHideFixedNavbar();
            }
        }
    }

    // Methods

    handlerMobileMenuToggle(target) {
        document.querySelector(target).classList.toggle("show");
    }

    handlerShowFixedNavbar() {
        this.mainHeader.classList.add("is-fixed-top");
    }

    handlerHideFixedNavbar() {
        this.mainHeader.classList.remove("is-fixed-top");
    }
}