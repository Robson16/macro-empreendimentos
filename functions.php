<?php

/**
 * Functions and Definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enqueuing of scripts and styles.
 */
function macro_scripts() {
    wp_enqueue_style( 'macro-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'macro-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_style( 'macro-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get( 'Version' ) );    

    wp_enqueue_script( 'macro-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get( 'Version' ), true );
}

add_action('wp_enqueue_scripts', 'macro_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function macro_gutenberg_scripts() {
    wp_enqueue_style( 'macro-webfonts-styles', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get( 'Version' ) );
    wp_enqueue_script( 'macro-editor-script', get_stylesheet_directory_uri() . '/assets/js/admin/editor.js', array( 'wp-blocks', 'wp-dom' ), '1.0', true );
}

add_action('enqueue_block_editor_assets', 'macro_gutenberg_scripts');


/** 
 * Set theme defaults and register support for various WordPress features.
 */
function macro_setup() {
    // Enabling translation support
    $textdomain = 'macro';
    load_theme_textdomain( $textdomain, get_stylesheet_directory() . '/languages/' );
    load_theme_textdomain( $textdomain, get_template_directory() . '/languages/' );

    // Load custom styles in the editor.
    add_theme_support( 'editor-styles' );
    add_editor_style( get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css' );

    // Customizable logo
    add_theme_support( 'custom-logo', array(
        'height'      => 85,
        'width'       => 150,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    // Menu registration
    register_nav_menus( array(
        'main_menu' => __( 'Main Menu', 'macro' ),
    ) );

    // Let WordPress manage the document title.
    add_theme_support( 'title-tag' );
    
    // Enable support for featured image on posts and pages.		 	
    add_theme_support( 'post-thumbnails' );

    // Enable support for embedded media for full weight
    add_theme_support( 'responsive-embeds' );

    // Enables wide and full dimensions
    add_theme_support( 'align-wide' );

    // Standard style for each block.
	add_theme_support( 'wp-block-styles' );

    // Disable custom colors
    add_theme_support( 'disable-custom-colors' );

    // Creates the specific color palette
    add_theme_support('editor-color-palette', array(
        array(
            'name'  => __( 'White', 'macro' ),
            'slug'  => 'white',
            'color'    => '#ffffff',
        ),        
        array(
            'name'  => __( 'Cadet Blue', 'macro' ),
            'slug'  => 'cadet-blue',
            'color'    => '#64a3a3',
        ),
        array(
            'name'  => __( 'Plum', 'macro' ),
            'slug'  => 'plum',
            'color'    => '#9a4288',
        ),
        array(
            'name'  => __( 'Imperial', 'macro' ),
            'slug'  => 'imperial',
            'color'    => '#60376f',
        ),
        array(
            'name'  => __( 'Dark Purple', 'macro' ),
            'slug'  => 'dark-purple',
            'color'    => '#2e1839',
        ),
        array(
            'name'  => __( 'Black', 'macro' ),
            'slug'  => 'black',
            'color'    => '#000000',
        ),
    ));

    // Disable custom gradients
    add_theme_support( 'disable-custom-gradients' );
}

add_action( 'after_setup_theme', 'macro_setup' );

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function macro_sidebars() {
    // Args used in all calls register_sidebar().
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );
    
    // Footer #1
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #1', 'macro' ),
        'id' => 'macro-sidebar-footer-1',
        'description' => __( 'The widgets in this area will be displayed in the first column in Footer.', 'macro' ),
    ) ) );

    // Footer #2
    register_sidebar( array_merge( $shared_args, array(
        'name' => __( 'Footer #2', 'macro' ),
        'id' => 'macro-sidebar-footer-2',
        'description' => __( 'The widgets in this area will be displayed in the second column in Footer.', 'macro' ),
    ) ) );

    // Footer #3
    register_sidebar(array_merge($shared_args, array(
        'name' => __( 'Footer #3', 'macro' ),
        'id' => 'macro-sidebar-footer-3',
        'description' => __( 'The widgets in this area will be displayed in the third column in Footer.', 'macro' ),
    )));
}

add_action( 'widgets_init', 'macro_sidebars' );

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  TGM Plugin Activation
 */
require_once get_template_directory() . '/includes/tgm-plugin-activation.php';
